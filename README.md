# GDoc-Trello-Integration:

It is an automation of the card creation in the Trello Boards from the google documents.

## In Google Apps Script
**Step 1:** Create a new project in Google Apps Script.

**Step 2:** Upload all the files in the Google Apps Script Project from the Google Script Directory.

**Step 3:** Replace the {API_KEY} in the index.html wiht your API Key.

**Step 4:** Publish the project as webapp and copy the link.

## In Trello Extension
**Step 1:** Paste the link in popup.html at line 25.

**Step 2:** Load the extension in Chrome.

**Step 3:** Open any google docs and proceed.